# stgc-mon
---
contact: **ssun@cern.ch** 
based on vmm-mon by Amilianos Koulouris 
https://gitlab.cern.ch/aikoulou/vmm-mon
aikoulou@cern.ch

Qt application to monitor sTGC chips, through the DAQ software found at https://gitlab.cern.ch/ssun/stgc_readout_sw

## Pre-requisites:

0. Linux! 
1. Qt 5.5 or greater (recommended 5.7, see [here](https://download.qt.io/archive/qt/5.7/5.7.0/))
2. ROOT 5.34 (find it [here](https://root.cern/content/release-53436))

## Run the monitoring!
Instructions can be found here:
https://indico.cern.ch/event/693304/contributions/2845167/attachments/1591515/2518875/MiniDAQ_SW_Tutorial_Run.pdf

```
git clone https://gitlab.cern.ch/ssun/sTGC_mon.git
cd sTGC_mon
qmake
make
./stgc-mon
```
