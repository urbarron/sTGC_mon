#ifndef MAINWINDOW_H
#define MAINWINDOW_H
//local
#include "chamber.h"
#include "chip.h"
#include "board.h"
#include "canvas.h"
#include "chip.h"
#include "chamber.h"
#include "vmm_decoder.h"
#include "struct/reply_type.h"
//QT includes
#include <QMainWindow>
#include <QUdpSocket>
#include <QWidget>
#include <QStringList>
#include <QTreeWidgetItem>
#include <QDebug>
#include <QDir>
#include <QTimer>
#include <QCloseEvent>
#include <QDir>
#include <QFile>
#include <QMessageBox>
#include <QDate>
#include <QFileDialog>
//C++
#include <fstream>
#include <sstream>
#include <string>
#include <iostream>
#include <iomanip>
#include <vector>

#include <cstdint>
#include <stdint.h>


namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

    void setInputBinary( std::string file ) { input_file_binary = file; }

private:
    void closeEvent(QCloseEvent *bar){exit(0);};
    void RunInputBinary();
    void Decode_and_Fill( std::vector<uint32_t> datagram, bool is_first_evt=false );

    void initHistos();

    unsigned long GetCurrentEVID( uint32_t l1id );

    void    PopUpError(const char * str, const char * info="");
    void    PopUpError(QString str, QString info);

    bool setupMetaFile();
    void getRunStartTime();
    void monitorEndRun();

    QString GetCurrentTimeStr();

    TH1D *h_event_time;

    std::string input_file_binary;
    VMMDecoder *decoder;

    FILE *ptr_myfile;
    ifstream input_file_meta;

    //--------------------------------

    long file_last_read_loc;
    int nMaxEventsRead;

    int nMaxEvents;
    int ievent;

    unsigned long first_evt_evid;
    unsigned long last_evid;
    unsigned long previous_evid;
    unsigned long previous_LB_evid;
    int nLB;
    std::vector<int> *nevt_vs_time;

    TString         m_ip;
    int             m_tdo;
    int             m_pdo;
    int             m_chan;
    int             m_vmm_id;
    int             m_bcid_rel;
    int             m_flag;
    int             m_board_id;
    int             m_bcid;
    uint32_t        m_l1_id;
    int             m_elink_id;


    Ui::MainWindow *ui;

    QUdpSocket *m_socket_receiver;

    QHostAddress fromIP;
    QByteArray incomingDatagram;
    int eventCount;
    int canvas_size_in_y;
    int canvas_size_in_x;
    bool isConfigured;
    bool debugMode;
    std::vector<Chamber*> chambers;
    std::vector<std::vector<Chip*>> chips;
    std::vector<Board*> boards;
    QMainCanvas* c_time;
    QMainCanvas* c_vmm;
    QMainCanvas* c_board;
    QMainCanvas* c_overview;
    int fill_counter;
    int last_trig_cnt;
    QString tmp_list1;
    std::vector<std::vector<QString>> config_table;
    std::vector<QString> config_row;
    QTimer* update_timer;
    QTimer* lumiblock_update_timer;
    QTimer* vmmC_update_timer;
    QTimer* boardC_update_timer;
    QTimer* overviewC_update_timer;
    int packets_recvd=0;
    int refreshRate=1000;
    int png_index=0;

    bool isPaused=false;

    QMessageBox msgBox;


private slots:

    void createCanvas();
    void setupCanvas();

    /// DATA HANDLING

    void fillVMM(int,int,int,int,int,int,int);
    void fillBoard(int,int,int,int,int,int,int);
    void updateData();
    void updateData_finishedRun();
    void updateLB();
    void updateVMM();
    void updateBoard();
    void updateOverview();

    /// DRAWING FUNCTIONS
    void viewVMM();
    void viewBoard();
    void viewOverview();
    void drawAllChips();

    void selectOutputDir();
    void stopMon();
    void startMon();

    void run_type_changed();

    void resetAllHistos();
    void deleteAllHistos();
    /// CANVAS UPDATE CONTROL --------------------------------------------------------------------------------------------------
    void startCanvasUpdates();
    void stopCanvasUpdates();
    void updatePads();
    void vmmC_updatePads();
    void boardC_updatePads();
    void overviewC_updatePads();
    /// UI CONTROL ---------------------------------------------------------------------------------------------
    void treeSelectionChanged();
    /// TOOLS --------------------------------------------
    void calculateCanvasSizeX();

    Chip* findChip(int vmm_id, int board_id);
    void printInfo();
    void debug(QString);

    void on_b_Reset_released();
    void on_b_Pause_released();
    void on_b_clearTreeSelection_released();
    void on_cb_showHit_released();
    void on_cb_showPDO_released();
    void on_cb_showTDO_released();
    void on_cb_showBCID_released();
    void on_cb_showEventSize_released();
    void on_combo_refreshRate_currentIndexChanged(int index);
    void on_b_exportPng_released();
    void on_b_exportPngAll_released();
    void on_b_exportPngAllBoards_released();
    void on_tabWidget_currentChanged(int index);

    void updateVmmInterface(int iboard, int ivmm);
    void updateBoardInterface(int iboard);

};
#endif // MAINWINDOW_H
